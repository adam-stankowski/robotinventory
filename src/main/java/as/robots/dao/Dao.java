package as.robots.dao;

import java.util.List;
import java.util.Optional;

public interface Dao <T> {
    T add(T item);
    Optional<T> getSingle(long id);
    void delete(long id);
    List<T> getAll();
}
