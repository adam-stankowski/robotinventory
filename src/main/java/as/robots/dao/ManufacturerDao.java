package as.robots.dao;

import as.robots.model.Manufacturer;

public interface ManufacturerDao extends Dao<Manufacturer> {
}
