package as.robots.dao;

import as.robots.model.Part;

import java.util.List;

public interface PartDao extends Dao<Part> {
    Part update(Part part);
    void delete(long partId);
    List<Part> getAllById(List<Long> ids);
    List<Part> getPartsCompatibleTo(long id);
}
