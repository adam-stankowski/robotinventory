package as.robots.dao;

import as.robots.model.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class PartDaoImpl extends BaseDao<Part> implements PartDao{
    public PartDaoImpl(@Autowired EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, Part.class);
    }

    @Override
    public Part update(Part part) {
        currentSession().merge(part);
        return part;
    }

    @Override
    public List<Part> getAllById(List<Long> ids) {
        return currentSession().createQuery("select p from Part p where p.id in :partIds")
                .setParameter("partIds", ids).list();
    }

    @Override
    public List<Part> getPartsCompatibleTo(long id) {
        return getSingle(id).map(part -> new ArrayList<>(part.getCompatibleParts())).orElseGet(() -> new ArrayList<>());
    }
}
