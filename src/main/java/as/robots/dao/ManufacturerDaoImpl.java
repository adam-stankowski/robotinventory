package as.robots.dao;

import as.robots.model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;

@Repository
@Transactional
public class ManufacturerDaoImpl extends BaseDao<Manufacturer> implements ManufacturerDao{

    public ManufacturerDaoImpl(@Autowired EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, Manufacturer.class);
    }
}
