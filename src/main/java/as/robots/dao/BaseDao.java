package as.robots.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Optional;

@Transactional
public abstract class BaseDao<T> implements Dao<T> {

    private final Class<T> entityClass;
    private final SessionFactory sessionFactory;

    public BaseDao(@Autowired EntityManagerFactory entityManagerFactory, Class<T> entityClass) {
        this.sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        this.entityClass = entityClass;
    }

    protected Session currentSession(){
        return sessionFactory.getCurrentSession();
    }

    @Override
    public T add(T item) {
        currentSession().saveOrUpdate(item);
        return item;
    }

    @Override
    public Optional<T> getSingle(long id) {
        return Optional.ofNullable(currentSession().get(entityClass, id));
    }

    @Override
    public void delete(long id) {
        getSingle(id).ifPresent((item) -> currentSession().delete(item));
    }

    @Override
    public List<T> getAll() {
        String select = String.format("from %s", entityClass.getName());
        return currentSession().createQuery(select).list();
    }
}
