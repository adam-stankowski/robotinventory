package as.robots.controller;

import as.robots.dto.PartDto;
import as.robots.model.Part;
import as.robots.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PartController {
    private final PartService partService;

    public PartController(@Autowired PartService partService) {
        this.partService = partService;
    }

    @RequestMapping(value = {"/parts/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<Part> getSingle(@PathVariable("id") long id) {
        Optional<Part> single = partService.getSingle(id);
        return single
                .map(part -> new ResponseEntity<>(part, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = {"/parts"}, method = RequestMethod.GET)
    public ResponseEntity<List<Part>> getAll() {
        return new ResponseEntity<>(partService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = {"/parts"}, method = RequestMethod.POST)
    public ResponseEntity<Part> add(@RequestBody PartDto partDto) {
        return new ResponseEntity<>(partService.add(partDto), HttpStatus.OK);
    }

    @RequestMapping(value = {"/parts"}, method = RequestMethod.PUT)
    public ResponseEntity<Part> update(@RequestBody PartDto partDto) {
        return new ResponseEntity<>(partService.update(partDto), HttpStatus.OK);
    }

    @RequestMapping(value = {"/parts/{id}"}, method = RequestMethod.DELETE)
    public ResponseEntity<Part> delete(@PathVariable("id") long id) {
        partService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = {"/parts/compatible/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<List<Part>> getPartsCompatibleTo(@PathVariable("id") long id) {
        List<Part> compatibleParts = partService.getPartsCompatibleTo(id);
        return new ResponseEntity<>(compatibleParts, HttpStatus.OK);
    }


}
