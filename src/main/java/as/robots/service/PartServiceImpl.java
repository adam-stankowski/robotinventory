package as.robots.service;

import as.robots.dao.ManufacturerDao;
import as.robots.dao.PartDao;
import as.robots.dto.PartDto;
import as.robots.model.Manufacturer;
import as.robots.model.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class PartServiceImpl implements PartService{
    private final PartDao partDao;
    private final ManufacturerDao manufacturerDao;

    public PartServiceImpl(@Autowired PartDao partDao, @Autowired ManufacturerDao manufacturerDao) {
        this.partDao = partDao;
        this.manufacturerDao = manufacturerDao;
    }

    private Part add(Part part) {
        return partDao.add(part);
    }

    private Part update(Part part) {
        return partDao.update(part);
    }

    @Override
    @Transactional
    public Part add(PartDto partDto) {
        Part part = new Part();
        mapToPartEntity(part, partDto);
        return add(part);
    }

    @Override
    @Transactional
    public Part update(PartDto partDto) {
        return getSingle(partDto.getId()).map(p -> {
            mapToPartEntity(p, partDto);
            return update(p);
        }).orElseThrow(() -> new IllegalArgumentException("Could not find Part with Id " + partDto.getId()));
    }

    @Override
    public Optional<Part> getSingle(long partId) {
        return partDao.getSingle(partId);
    }

    @Override
    public void delete(long partId) {
        partDao.delete(partId);
    }

    @Override
    public List<Part>getAll() {
        return partDao.getAll();
    }

    @Override
    public List<Part> getPartsCompatibleTo(long id) {
        return partDao.getPartsCompatibleTo(id);
    }

    private void mapToPartEntity(Part destination, PartDto partDtoSource) {
        Manufacturer manufacturer = manufacturerDao.getSingle(partDtoSource.getManufacturerId())
                .orElseThrow(() -> new IllegalArgumentException("Manufacturer not found"));
        List<Part> compatibleParts = partDao.getAllById(partDtoSource.getCompatiblePartsIds());
        destination.setName(partDtoSource.getName());
        destination.setSerialNo(partDtoSource.getSerialNo());
        destination.setWeightKg(partDtoSource.getWeightKg());
        destination.setManufacturer(manufacturer);
        destination.setCompatibleParts(new HashSet<>(compatibleParts));
    }
}
