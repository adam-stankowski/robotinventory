package as.robots.service;

import as.robots.dto.PartDto;
import as.robots.model.Part;

import java.util.List;
import java.util.Optional;

public interface PartService {
    Part add(PartDto partDto);
    Part update(PartDto part);
    Optional<Part> getSingle(long partId);
    void delete(long partId);
    List<Part> getAll();
    List<Part> getPartsCompatibleTo(long id);
}
