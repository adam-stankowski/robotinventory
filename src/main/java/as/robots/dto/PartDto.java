package as.robots.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PartDto {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("serial_no")
    private String serialNo;
    @JsonProperty("manufacturer_id")
    private Long manufacturerId;
    @JsonProperty("weight_kg")
    private Double weightKg;
    @JsonProperty("compatible_part_ids")
    private List<Long> compatiblePartsIds;

    public PartDto() {
    }

    public PartDto(String name, String serialNo, Long manufacturerId, Double weightKg, List<Long> compatiblePartsIds) {
        this.name = name;
        this.serialNo = serialNo;
        this.manufacturerId = manufacturerId;
        this.weightKg = weightKg;
        this.compatiblePartsIds = compatiblePartsIds;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public void setWeightKg(Double weightKg) {
        this.weightKg = weightKg;
    }

    public void setCompatiblePartsIds(List<Long> compatiblePartsIds) {
        this.compatiblePartsIds = compatiblePartsIds;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public Long getManufacturerId() {
        return manufacturerId;
    }


    public Double getWeightKg() {
        return weightKg;
    }

    public List<Long> getCompatiblePartsIds() {
        return compatiblePartsIds;
    }
}
