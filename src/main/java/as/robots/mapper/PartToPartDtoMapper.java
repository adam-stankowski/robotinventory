package as.robots.mapper;

import as.robots.dto.PartDto;
import as.robots.model.Part;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PartToPartDtoMapper {
    public static PartDto toDto(Part part) {
        Set<Part> compatibleParts = part.getCompatibleParts();
        List<Long> compatiblePartIds = compatibleParts.stream().map(Part::getId).collect(Collectors.toList());
        return new PartDto(part.getName(), part.getSerialNo(), part.getManufacturer().getId(), part.getWeightKg(), compatiblePartIds);
    }

    public static List<PartDto> toDtoList(List<Part> parts) {
        return parts.stream().map(PartToPartDtoMapper::toDto).collect(Collectors.toList());
    }

}
