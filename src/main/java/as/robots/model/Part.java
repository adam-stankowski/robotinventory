package as.robots.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Part {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 100)
    private String name;
    @Column(length = 30, name = "serial_no")
    private String serialNo;
    @ManyToOne
    @JoinColumn(name="manufacturer_id")
    private Manufacturer manufacturer;
    @Column(name = "weight_kg")
    private Double weightKg;
    @ManyToMany(cascade = {CascadeType.REMOVE})
    @JoinTable(name = "Part_Part")
    private Set<Part> compatibleParts;

    public Part() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(Double weightKg) {
        this.weightKg = weightKg;
    }

    public Set<Part> getCompatibleParts() {
        return compatibleParts;
    }

    public void setCompatibleParts(Set<Part> compatibleParts) {
        this.compatibleParts = compatibleParts;
    }
}
