package as.robots.dao;

import as.robots.model.Part;
import org.hamcrest.MatcherAssert;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

public class PartDaoTest {

    private PartDao partDao;
    private SessionFactory sessionFactory;
    private Session currentSession;
    private Query query;

    @Before
    public void setUp() {
        sessionFactory = Mockito.mock(SessionFactory.class);
        currentSession = Mockito.mock(Session.class);
        query = Mockito.mock(Query.class);
        EntityManagerFactory entityManagerFactory = Mockito.mock(EntityManagerFactory.class);
        Mockito.when(entityManagerFactory.unwrap(Mockito.any())).thenReturn(sessionFactory);
        Mockito.when(sessionFactory.getCurrentSession()).thenReturn(currentSession);
        Mockito.when(currentSession.createQuery(anyString())).thenReturn(query);
        partDao = new PartDaoImpl(entityManagerFactory);
    }

    @Test
    public void whenAddThenSaveOnSession() {
        Part part = new Part();
        part.setName("New Part");
        partDao.add(part);
        Mockito.verify(currentSession, Mockito.times(1)).saveOrUpdate(anyObject());
    }

    @Test
    public void whenUpdateThenMergeOnSession() {
        Part part = new Part();
        part.setName("New Part");
        partDao.update(part);
        Mockito.verify(currentSession, Mockito.times(1)).merge(anyObject());
    }

    @Test
    public void whenItemNotExistsThenEmptyOptional() {
        Mockito.when(currentSession.get(Part.class, 1)).thenReturn(null);
        Optional<Part> single = partDao.getSingle(1);
        MatcherAssert.assertThat(single.isPresent(), is(false));
    }

    @Test
    public void whenItemExistsThenOptionalWithValue() {
        String partName = "PartName";
        Part part = new Part();
        part.setName(partName);
        Mockito.when(currentSession.get(Part.class, 1L)).thenReturn(part);
        Optional<Part> single = partDao.getSingle(1);
        MatcherAssert.assertThat(single.isPresent(), is(true));
        MatcherAssert.assertThat(single.get().getName(), is(partName));
    }

    @Test
    public void whenNoItemsThenGetAllIsEmptyList() {
        Mockito.when(query.list()).thenReturn(new ArrayList());
        List<Part> all = partDao.getAll();
        MatcherAssert.assertThat(all, empty());
    }

    @Test
    public void whenItemsExistThenGetAllReturnsThem() {
        Part part1 = new Part();
        Part part2 = new Part();
        Mockito.when(query.list()).thenReturn(Arrays.asList(part1, part2));
        List<Part> all = partDao.getAll();
        MatcherAssert.assertThat(all, hasSize(2));
        MatcherAssert.assertThat(all, containsInAnyOrder(part1, part2));
    }

    @Test
    public void whenItemExistsThenDeleteRemovesItem() {
        Part part = new Part();
        Mockito.when(currentSession.get(Part.class, 1L)).thenReturn(part);
        partDao.delete(1);
        Mockito.verify(currentSession, Mockito.times(1)).delete(part);
    }

    @Test
    public void whenItemNotExistsThenNoAction() {
        Part part = new Part();
        Mockito.when(currentSession.get(Part.class, 1L)).thenReturn(null);
        partDao.delete(1);
        Mockito.verify(currentSession, Mockito.never()).delete(anyObject());
    }
}